import pytest

from wrap import wrap


@pytest.mark.parametrize("input", [
    ("", 1, ""),
    ("a", 1, "a"),
    ("ab", 1, "a\nb"),
    ("abc", 1, "a\nb\nc"),
    ("a b", 1, "a\nb"),
    ("a bc", 3, "a\nbc"),
    ("a b c e", 3, "a b\nc e"),
    ("a dog with a bone", 6, "a dog\nwith a\nbone")
])
def test_wrap_empty_text(input):
    input_text, width, expected = input
    assert expected == wrap(input_text, width)
