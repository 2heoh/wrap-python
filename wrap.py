def wrap(input_text, width):
    break_point = input_text[:width+1].rfind(" ")
    if break_point == -1:
        break_point = width

    if len(input_text) > width:
        return input_text[:break_point] + "\n" + wrap(input_text[break_point:].strip(), width)
    return input_text
